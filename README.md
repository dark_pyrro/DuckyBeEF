# DuckyBeEF

* Author:        dark_pyrro
* Version:       1.0
* Target:        Windows
* Props:         None really, perhaps NetworkChuck and his BeEF video that got me thinking "hmmmm, that could be triggered by the Ducky" (or Bunny, or Croc.....)
* Category:      "Computer remote control" (ish)

## Requirements
A running BeEF instance somewhere in the world possible to access by the target computer that the USB Rubber Ducky is "abusing". Encode the payload.txt file to an inject.bin file using the official jsencoder.

## Description
Opens the Edge browser and visits an address that is hosted/served by a BeEF instance.
Any BeEF functionality that is possible to use on the target should be available.

This PoC/sample payload script opens the Edge browser with a minimized incognito window. It's of course possible to change this depending on what goals a specific engagement has. The web page used in the payload is the sample butcher page that comes with BeEF.